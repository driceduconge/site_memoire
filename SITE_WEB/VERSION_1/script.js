// systeme d'indexation des entrées
const sections = document.querySelectorAll("section");
sections.forEach(element => {
    let section_name;
    switch(element.id){
        case "section_a":
            section_name = "a";
            break;
        case "section_b":
            section_name = "b";
            break;
        case "section_c":
            section_name = "c";
            break;
    }
    let list_hr = element.querySelectorAll(":scope > hr");

    for (let index = 0; index < list_hr.length; index++) {
        const element = list_hr[index];
        element.id = section_name+(index+1);
    }
});
